import test from 'ava';
import mongoose from 'mongoose';
import initModels from '../../../src/models';
import { Poll as PollFactory } from '../../helpers/factories';

const models = initModels(mongoose);
const { Poll } = models;

test('Cannot create poll with duplicate options', (t) => {
  t.plan(1);

  const poll = new Poll(PollFactory.build({
    options: ['duplicate', 'duplicate'],
  }));

  const err = poll.validateSync();

  t.truthy(err);
});

test('Cannot create a poll with less than two options', (t) => {
  t.plan(1);

  const poll = new Poll(PollFactory.build({
    options: ['option'],
  }));

  const err = poll.validateSync();

  t.truthy(err);
});

test('Duplicate options with different casing are acceptable', (t) => {
  t.plan(1);

  const poll = new Poll(PollFactory.build({
    options: ['duplicate', 'Duplicate'],
  }));

  const err = poll.validateSync();

  t.falsy(err);
});
