/* eslint-disable no-param-reassign */

import test from 'ava';
import request from 'supertest';
import { setup, userJWT, teardownDatabase } from '../../../helpers';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('It should respond with 401 if no token is provided', async (t) => {
  t.plan(1);

  const res = await request(t.context.app)
    .get('/api/profile');

  t.is(res.status, 401);
});

test('It should repond with a user profile with a valid token', async (t) => {
  t.plan(2);

  const res = await request(t.context.app)
    .get('/api/profile')
    .set('Cookie', `token=${userJWT({ username: 'test' })}`)
    .expect(200);

  t.truthy(res.body.username);
  t.falsy(res.body.auth);
});
