/* eslint-disable no-param-reassign */

import test from 'ava';
import request from 'supertest';
import { setup, teardownDatabase } from '../../helpers';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('It should respond with HTML for routes non-api routes', async (t) => {
  t.plan(2);

  const res = await request(t.context.app)
    .get('/');

  t.is(res.status, 200);
  t.truthy(res.header['content-type'].startsWith('text/html'));
});
