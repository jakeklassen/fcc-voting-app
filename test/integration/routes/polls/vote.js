/* eslint-disable no-param-reassign */

import test from 'ava';
import request from 'supertest';
import { setup, userJWT, teardownDatabase } from '../../../helpers';
import { Poll as PollFactory, User as UserFactory } from '../../../helpers/factories';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;

  const User = t.context.db.model('User');

  t.context.user = await User.create(UserFactory.build());
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('Anyone can vote', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .send({
      option: poll.options[0],
    })
    .expect(200);

  const existingPoll = await Poll.findById(poll.id);
  t.is(existingPoll.votes.length, 1);
});

test('Guests cannot vote with a custom option', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .send({
      option: 'custom',
      custom: true,
    })
    .expect(403);

  const existingPoll = await Poll.findById(poll.id);
  t.is(existingPoll.votes.length, 0);
});

test('Users can vote with a custom option', async (t) => {
  t.plan(2);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .send({
      option: 'custom',
      custom: true,
    })
    .expect(200);

  const existingPoll = await Poll.findById(poll.id);
  t.is(existingPoll.votes.length, 1);
  t.truthy(existingPoll.options.includes('custom'));
});

test('You cannot vote a non-existent options', async (t) => {
  t.plan(2);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .send({
      option: 'custom',
    })
    .expect(403);

  const existingPoll = await Poll.findById(poll.id);
  t.is(existingPoll.votes.length, 0);
  t.falsy(existingPoll.options.includes('custom'));
});

test('You cannot vote twice', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  // Vote
  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .send({
      option: poll.options[0],
    })
    .expect(200);

  // Try again - fails
  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .send({
      option: poll.options[0],
    })
    .expect(403);

  const existingPoll = await Poll.findById(poll.id);
  t.is(existingPoll.votes.length, 1);
});

test('A poll will indicate I cannot vote if I have already as a guest', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  // Vote
  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .send({
      option: poll.options[0],
    })
    .expect(200);

  const res = await request(t.context.app)
    .get(`/api/polls/${poll.id}`)
    .expect(200);

  t.is(res.body.poll.canVote, false);
});

test('A poll will indicate I cannot vote if I voted while signed in', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  // Vote
  await request(t.context.app)
    .post(`/api/polls/${poll.id}/vote`)
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .send({
      option: poll.options[0],
    })
    .expect(200);

  const res = await request(t.context.app)
    .get(`/api/polls/${poll.id}`)
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .expect(200);

  t.is(res.body.poll.canVote, false);
});
