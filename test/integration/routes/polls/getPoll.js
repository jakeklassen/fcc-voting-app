/* eslint-disable no-param-reassign */

import test from 'ava';
import { Types } from 'mongoose';
import request from 'supertest';
import { stub } from 'sinon';
import { setup, teardownDatabase } from '../../../helpers';
import { Poll as PollFactory, User as UserFactory } from '../../../helpers/factories';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;

  const User = t.context.db.model('User');

  t.context.user = await User.create(UserFactory.build());
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('Anyone can request a poll', async (t) => {
  t.plan(2);

  const Poll = t.context.db.model('Poll');
  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  const res = await request(t.context.app)
    .get(`/api/polls/${poll.id}`)
    .expect(200);

  t.truthy(res.body.poll);
  t.true(res.body.poll.canVote);
});

// Mongoose will throw a CastError on poll ids that are not ObjectId's
// We still want this to just be a 404
test('Should 404 on bad ObjectId', async (t) => {
  t.plan(1);

  await request(t.context.app)
    .get('/api/polls/1234')
    .expect(404);

  t.pass();
});

test('Should 404 on non-existent poll', async (t) => {
  t.plan(1);

  await request(t.context.app)
    .get(`/api/polls/${Types.ObjectId()}`)
    .expect(404);

  t.pass();
});

test('Should catch errors', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');
  const findByIdSpy = stub(Poll, 'findById').callsFake(() => Promise.reject(new Error()));

  // Trigger findById error
  await request(t.context.app)
    .get(`/api/polls/${Types.ObjectId()}`)
    .expect(500);

  t.pass();
  findByIdSpy.restore();
});
