/* eslint-disable no-param-reassign */

import test from 'ava';
import request from 'supertest';
import { setup, userJWT, teardownDatabase } from '../../../helpers';
import { Poll as PollFactory, User as UserFactory } from '../../../helpers/factories';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;

  const User = t.context.db.model('User');

  t.context.user = await User.create(UserFactory.build());
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('Anyone can request polls', async (t) => {
  t.plan(1);

  const res = await request(t.context.app)
    .get('/api/polls')
    .expect(200);

  t.truthy(res.body.polls);
});

test('As an authenticated user I can get my own polls', async (t) => {
  t.plan(1);

  // Create as the context.user
  await request(t.context.app)
    .post('/api/polls')
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .send(PollFactory.build())
    .expect(201);

  const res = await request(t.context.app)
    .get('/api/polls')
    .query({
      mine: true,
    })
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .expect(200);

  t.is(res.body.polls.length, 1);
});

test('When not logged in fetching my polls has no effect', async (t) => {
  t.plan(1);

  // Create as the context.user
  await request(t.context.app)
    .post('/api/polls')
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .send(PollFactory.build())
    .expect(201);

  const res = await request(t.context.app)
    .get('/api/polls')
    .query({
      mine: true,
    })
    .expect(200);

  t.not(res.body.polls.length, 0);
});
