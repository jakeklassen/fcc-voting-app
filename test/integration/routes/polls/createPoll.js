/* eslint-disable no-param-reassign */

import test from 'ava';
import mongoose from 'mongoose';
import request from 'supertest';
import { setup, userJWT, teardownDatabase } from '../../../helpers';
import { Poll as PollFactory, User as UserFactory } from '../../../helpers/factories';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;

  const User = t.context.db.model('User');

  t.context.user = await User.create(UserFactory.build());
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('As a guest, I cannot create a poll', async (t) => {
  t.plan(1);

  await request(t.context.app)
    .post('/api/polls')
    .send({})
    .expect(401);

  t.pass();
});

test('The creator of the poll must exist', async (t) => {
  t.plan(1);

  const doesNotExist = UserFactory.build();

  const res = await request(t.context.app)
    .post('/api/polls')
    .set('cookie', `token=${userJWT(doesNotExist)}`)
    .send(PollFactory.build())
    .expect(400);

  t.falsy(res.body.poll);
});

test('As an authorized user, I can create a poll', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const res = await request(t.context.app)
    .post('/api/polls')
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .send(PollFactory.build({ creator: t.context.user.id }))
    .expect(201);

  t.truthy(await Poll.findById(res.body.poll._id));
});

test('Should handle polls that fail validation', async (t) => {
  t.plan(1);

  await request(t.context.app)
    .post('/api/polls')
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .send(PollFactory.build({ options: [] }))
    .expect(400);

  t.pass();
});
