/* eslint-disable no-param-reassign */

import test from 'ava';
import request from 'supertest';
import { setup, userJWT, teardownDatabase } from '../../../helpers';
import { Poll as PollFactory, User as UserFactory } from '../../../helpers/factories';

test.beforeEach(async (t) => {
  const { app, db } = await setup();
  t.context.app = app;
  t.context.db = db;

  const User = t.context.db.model('User');

  t.context.user = await User.create(UserFactory.build());
});

test.afterEach('cleanup', async (t) => {
  await teardownDatabase(t.context.db.connection);
});

test('If I am the creator, poll will indicate that I can delete', async (t) => {
  t.plan(2);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  const res = await request(t.context.app)
    .get(`/api/polls/${poll.id}`)
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .expect(200);

  t.truthy(res.body.poll);
  t.true(res.body.poll.canDelete);
});

test('If I am not the creator, poll will indicate that I cannot delete', async (t) => {
  t.plan(2);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  const res = await request(t.context.app)
    .get(`/api/polls/${poll.id}`)
    .expect(200);

  t.truthy(res.body.poll);
  t.false(res.body.poll.canDelete);
});

test('As the creator of a poll, I can delete it', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  await request(t.context.app)
    .delete(`/api/polls/${poll.id}`)
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .expect(200);

  t.falsy(await Poll.findById(poll.id));
});

test('If I am not the poll creator, I can delete it', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');
  const User = t.context.db.model('User');

  const user = await User.create(UserFactory.build());
  const poll = await Poll.create(PollFactory.build({
    creator: user.id,
  }));

  await request(t.context.app)
    .delete(`/api/polls/${poll.id}`)
    .set('cookie', `token=${userJWT(t.context.user)}`)
    .expect(401);

  t.truthy(await Poll.findById(poll.id));
});

test('Guests cannot delete polls', async (t) => {
  t.plan(1);

  const Poll = t.context.db.model('Poll');

  const poll = await Poll.create(PollFactory.build({
    creator: t.context.user.id,
  }));

  await request(t.context.app)
    .delete(`/api/polls/${poll.id}`)
    .expect(401);

  t.truthy(await Poll.findById(poll.id));
});
