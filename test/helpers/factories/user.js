import { Factory } from 'rosie';
import Chance from 'chance';
import { Types } from 'mongoose';

const chance = new Chance();

export default new Factory()
  .attr('_id', () => Types.ObjectId())
  .attr('username', () => `${chance.twitter()}-${chance.word()}`)
  .attr('auth', ['username'], username => ({
    twitter: {
      id: chance.natural({ min: 10000, max: 100000 }),
      username,
    },
  }));
