import { Factory } from 'rosie';
import Chance from 'chance';
import { Types } from 'mongoose';
import User from './user';

const chance = new Chance();

export default new Factory()
  .attr('_id', () => Types.ObjectId())
  .attr('creator', () => User.build())
  .attr('title', () => chance.sentence())
  .attr('options', () => chance.unique(chance.word, 5))
  .attr('votes', []);
