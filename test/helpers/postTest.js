/* eslint-disable no-console */

import 'dotenv/config';
import { MongoClient } from 'mongodb';

// This script will clean up the test databases

// Add to package.json scripts to run
// "posttest": "cross-env NODE_ENV=test node -r babel-register ./test/helpers/postTest.js"

const MONGO_URL = 'mongodb://localhost';

MongoClient.connect(MONGO_URL, async (err, db) => {
  if (err) throw err;

  // Load all databases
  const { databases } = await db.admin().listDatabases();
  // Find those matching `fcc-voting-app-test-[-xxx]` pattern
  const toBeDropped = databases.filter(({ name }) => name.match(/^fcc-voting-app-test(-.*)?$/));
  // Connect to all databases
  const connections = await Promise.all(
    toBeDropped.map(({ name }) => MongoClient.connect(`${MONGO_URL}/${name}`)),
  );

  // Drop all databases
  await Promise.all(connections.map(connection => connection.dropDatabase()));

  // Close all connections
  await Promise.all(connections.map(connection => connection.close()));
  await db.close();

  console.log('Test databases dropped');
});
