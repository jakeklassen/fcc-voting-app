import jsonwebtoken from 'jsonwebtoken';
import mongoose from 'mongoose';
import shortid from 'shortid';
import init from '../../src/app';

const { GITLAB_CI } = process.env;
mongoose.Promise = Promise;

export const setup = async () => {
  const db = new mongoose.Mongoose();
  await db.connect(
    `mongodb://${GITLAB_CI === 'true' ? 'mongo' : 'localhost'}/fcc-voting-app-test-${shortid.generate()}`,
    { config: { autoIndex: false } },
  );

  const app = init({ mongoose: db });

  // Copy models
  for (const name of mongoose.modelNames()) {
    db.model(name, mongoose.model(name).schema);
  }

  return { app, db };
};

export const teardownDatabase = async (connection) => {
  // Force drop `sessions` from `connect-mongo`. It seems to hang around after
  // tests.
  // I'll look into the source repo.
  await connection.db.dropCollection('sessions');
  await connection.dropDatabase();
  await connection.close();
};

export const userJWT = (
  user = {},
  secret = 'secret',
  config = {
    expiresIn: '1h',
  }) => jsonwebtoken.sign(user.toObject ? user.toObject() : user, secret, config);
