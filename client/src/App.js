import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { Grid, Sidebar } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import 'whatwg-fetch';
import './App.css';
import Navigation from './components/Navigation';
import Polls from './components/Polls';
import NewPoll from './components/NewPoll';
import Poll from './components/Poll';

// Closable Sidebar feature coming
// https://github.com/Semantic-Org/Semantic-UI-React/pull/1473
// Unitl then use workaround

// To pass props to the PrivateRoute Component, set props={{ prop: 'val' }}
const PrivateRoute = ({ component: Component, ...rest }) =>
  <Route
    {...rest}
    render={props => {
      return rest.user.isAuthenticated
        ? <Component {...props} {...rest.props} />
        : <Redirect to={{ pathname: '/', state: { from: props.location } }} />;
    }}
  />;

class App extends Component {
  state = {
    user: {
      isAuthenticated: false,
      profile: null,
    },
  };

  async componentWillMount() {
    const profile = await fetch('/api/profile', {
      credentials: 'same-origin',
    }).then(res => res.json());

    this.setState({
      ...this.state,
      user: {
        isAuthenticated: profile.statusCode === 401 ? false : true,
        profile: profile.statusCode === 401 ? null : profile,
      },
    });
  }

  async fetchPolls(mine = false) {
    const { polls = [] } = await fetch(`/api/polls?mine=${mine}`, {
      credentials: 'include',
    })
      .then(res => res.json())
      .catch(error => console.error(error));

    return polls;
  }

  login(e) {
    window.location = '/auth/twitter';
  }

  async logout() {
    const res = await fetch('/auth/logout', {
      credentials: 'same-origin',
    });

    if (res.status === 401) {
      console.log('unauthorized');
    } else {
      console.log('signed out');

      this.setState({
        ...this.state,
        user: {
          isAuthenticated: false,
          profile: null,
        },
      });

      // Redirect to index if not there already
      if (window.location.pathname !== '/') this.props.history.push('/');
    }
  }

  render() {
    return (
      <Sidebar.Pushable>
        <Navigation
          user={this.state.user}
          logout={this.logout.bind(this)}
          login={this.login}
        />

        <Sidebar.Pusher as={Grid} centered container>
          <Grid.Row
            style={{ minHeight: 'calc(100vh - 103px)', paddingBottom: '3em' }}
          >
            <Switch>
              <Route exact path="/">
                <Polls user={this.state.user} fetchPolls={this.fetchPolls} />
              </Route>
              <Route
                exact
                path="/polls/:id"
                render={props => <Poll user={this.state.user} {...props} />}
              />
              <PrivateRoute
                exact
                path="/my_polls"
                component={Polls}
                user={this.state.user}
                props={{
                  fetchPolls: this.fetchPolls,
                  mine: true,
                  user: this.state.user,
                }}
              />
              <PrivateRoute
                exact
                path="/new_poll"
                user={this.state.user}
                component={NewPoll}
              />
            </Switch>
          </Grid.Row>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}

export default withRouter(App);
