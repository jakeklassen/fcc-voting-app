import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Dropdown,
  Dimmer,
  Loader,
  Modal,
  Input,
} from 'semantic-ui-react';
import { HorizontalBar } from 'react-chartjs-2';
import randomColor from 'randomcolor';
import 'whatwg-fetch';
import './Poll.css';

export default class Poll extends Component {
  state = {
    poll: undefined,
    selection: '',
    custom: false,
    customValue: '',
    isDeleteModalOpen: false,
    isErrorModalOpen: false,
    errorMessage: '',
  };

  static propTypes = {
    user: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  async fetchPoll(id) {
    const { poll } = await fetch(`/api/polls/${id}`, {
      credentials: 'include',
    })
      .then(res => res.json())
      .catch(error => console.error(error));

    return poll;
  }

  async componentWillMount() {
    const poll = await this.fetchPoll(this.props.match.params.id);

    this.setState({
      ...this.state,
      poll,
      colors: randomColor({ count: poll.votes.length }),
    });
  }

  closeDeleteModal = () => {
    this.setState({
      ...this.state,
      isDeleteModalOpen: false,
    });
  };

  showDeleteModal = () => {
    this.setState({
      ...this.state,
      isDeleteModalOpen: true,
    });
  };

  showErrorModal = message => {
    this.setState({
      ...this.state,
      isErrorModalOpen: true,
      errorMessage: message,
    });
  };

  closeErrorModal = () => {
    this.setState({
      ...this.state,
      isErrorModalOpen: false,
    });
  };

  handleVote = async () => {
    const { selection, custom, customValue } = this.state;
    try {
      if (!custom && !this.state.poll.options.includes(selection)) {
        this.showErrorModal('Invalid option');
      } else if (custom && this.isVotingDisabled()) {
        this.showErrorModal('You cannot vote on this poll');
      } else {
        const { poll } = await fetch(`/api/polls/${this.state.poll._id}/vote`, {
          method: 'post',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            option: custom ? customValue : selection,
            custom: custom,
          }),
        }).then(res => res.json());

        this.setState({
          ...this.state,
          poll,
          selection: custom ? customValue : selection,
        });
      }
    } catch (error) {
      console.error(error);
      alert('There was an error submitting your vote');
    }
  };

  handleDelete = async () => {
    try {
      await fetch(`/api/polls/${this.state.poll._id}`, {
        method: 'delete',
        credentials: 'include',
      });

      this.props.history.push('/');
    } catch (error) {
      console.error(error);
      alert('Error deleting poll');
    }
  };

  handleOptionSelect(e, { value }) {
    this.setState({
      ...this.state,
      selection: value,
      custom: value === 'custom' ? true : false,
      customValue: '',
    });
  }

  updateCustomOption = (e, { value }) =>
    this.setState({
      ...this.state,
      customValue: value,
    });

  isVotingDisabled = () => {
    if (this.state.custom && this.state.customValue.trim().length === 0) {
      return true;
    }

    return this.state.poll.canVote ? false : true;
  };

  render() {
    if (!this.state.poll) {
      return (
        <Container textAlign="center">
          <Dimmer active inverted>
            <Loader inverted />
          </Dimmer>
        </Container>
      );
    }

    const {
      poll,
      colors,
      selection,
      isErrorModalOpen,
      errorMessage,
      isDeleteModalOpen,
    } = this.state;
    // We want to show all options, even if no votes exist for it (as 0).
    const votes = poll.options.map(option => [
      option,
      poll.votes.reduce(
        (acc, { value }) => acc + (value === option ? 1 : 0),
        0,
      ),
    ]);
    votes.sort(([_a, aCount], [_b, bCount]) => bCount - aCount);

    const data = {
      labels: votes.map(([value]) => value),
      datasets: [
        {
          label: 'Votes',
          data: votes.map(([value, count]) => count),
          backgroundColor: colors,
          hoverBackgroundColor: colors,
        },
      ],
    };

    const dropdownOptions = [
      ...poll.options.map(option => ({
        key: option,
        text: option,
        value: option,
      })),
    ];

    if (this.props.user.isAuthenticated) {
      dropdownOptions.push({
        key: 'custom',
        text: 'Add Custom Option',
        value: 'custom',
      });
    }

    return (
      <Grid stackable container doubling>
        <Grid.Row>
          <Grid.Column>
            <Header as="h2" icon textAlign="center">
              <Header.Content>
                <Divider hidden />
                {poll.title}
              </Header.Content>
              <Header.Subheader>
                {poll.canVote ? '' : 'You already voted'}
              </Header.Subheader>
            </Header>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row centered>
          <Grid.Column mobile={16} computer={6} tablet={8}>
            <Dropdown
              style={{ width: '100%' }}
              placeholder="Select option"
              selection
              text={selection}
              onChange={(e, data) => this.handleOptionSelect(e, data)}
              options={dropdownOptions}
            />
          </Grid.Column>
        </Grid.Row>

        {poll.canVote && selection === 'custom'
          ? <Grid.Row centered>
              <Grid.Column mobile={16} computer={6} tablet={8}>
                <Input
                  disabled={poll.canVote === false}
                  placeholder="Custom option"
                  style={{ width: '100%' }}
                  onChange={this.updateCustomOption}
                />
              </Grid.Column>
            </Grid.Row>
          : ''}

        <Grid.Row centered columns={3} id="poll-buttons">
          <Button
            disabled={this.isVotingDisabled()}
            onClick={this.handleVote}
            color="green"
            content="Vote"
            icon="heart"
            label={{
              basic: true,
              pointing: 'left',
              content: poll.votes.length,
            }}
          />

          <Button color="twitter" style={{ marginLeft: '0.2em' }}>
            <Icon name="twitter" /> Share
          </Button>

          {poll.canDelete
            ? <Modal
                open={isDeleteModalOpen}
                onClose={this.closeDeleteModal}
                trigger={
                  <Button
                    color="red"
                    content="Delete"
                    icon="close"
                    onClick={this.showDeleteModal}
                  />
                }
                basic
                size="small"
              >
                <Header icon="remove" content="Delete Poll" />
                <Modal.Content>
                  <p>Are you sure you want to delete this poll?</p>
                </Modal.Content>
                <Modal.Actions>
                  <Button
                    basic
                    color="red"
                    onClick={this.closeDeleteModal}
                    inverted
                  >
                    <Icon name="remove" /> No
                  </Button>
                  <Button color="green" onClick={this.handleDelete} inverted>
                    <Icon name="checkmark" /> Yes
                  </Button>
                </Modal.Actions>
              </Modal>
            : ''}
        </Grid.Row>

        {isErrorModalOpen
          ? <Grid.Row>
              <Grid.Column>
                <Modal open onClose={this.closeErrorModal} basic size="small">
                  <Header icon="warning" content="Error" />
                  <Modal.Content>
                    <p>{errorMessage}</p>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button
                      color="green"
                      onClick={this.closeErrorModal}
                      inverted
                    >
                      <Icon name="checkmark" /> OK
                    </Button>
                  </Modal.Actions>
                </Modal>
              </Grid.Column>
            </Grid.Row>
          : ''}

        <Grid.Row>
          <Grid.Column>
            <HorizontalBar data={data} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
