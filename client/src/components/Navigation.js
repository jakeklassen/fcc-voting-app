import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Divider,
  Grid,
  Menu,
  Icon,
  Sidebar,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const LogoHeader = props =>
  <Menu.Item
    as={Link}
    to="/"
    id="header"
    onClick={e => props.handleItemClick(e, { name: 'home' })}
  >
    <h3>FCC Voting Zipline</h3>
  </Menu.Item>;

const LoginMenuItem = props =>
  <Menu.Item link onClick={props.login}>Login</Menu.Item>;

LoginMenuItem.propTypes = {
  login: PropTypes.func.isRequired,
};

const LogoutMenuItem = props =>
  <Menu.Item link onClick={props.logout}>
    Logout ({props.user.username})
  </Menu.Item>;

LogoutMenuItem.propTypes = {
  logout: PropTypes.func.isRequired,
};

const MyPollsMenuItem = props =>
  <Menu.Item
    as={Link}
    to="/my_polls"
    active={props.active}
    onClick={e => props.handleItemClick(e, { name: 'my_polls' })}
  >
    My Polls
  </Menu.Item>;

const NewPollMenuItem = props =>
  <Menu.Item
    as={Link}
    to="/new_poll"
    active={props.active}
    onClick={e => props.handleItemClick(e, { name: 'new_poll' })}
  >
    New Poll
  </Menu.Item>;

class Navigation extends Component {
  state = {
    activeMenuItem: 'home',
    sidebarVisible: false,
  };

  static propTypes = {
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
  };

  handleItemClick = (e, { name }) => {
    this.setState({
      ...this.state,
      activeMenuItem: name,
      sidebarVisible: false,
    });
  };

  toggleSidebar = e =>
    this.setState({
      ...this.state,
      sidebarVisible: !this.state.sidebarVisible,
    });

  hideSidebar = () =>
    this.setState({
      ...this.state,
      sidebarVisible: false,
    });

  handleLogout = async () => {
    this.hideSidebar();
    await this.props.logout();
  };

  render() {
    const { activeMenuItem, sidebarVisible } = this.state;
    const { isAuthenticated, profile } = this.props.user;

    return (
      <div id="navbar">
        <Grid as={Container} centered>
          <Grid.Column stretched width={16} only="mobile">
            <Menu secondary>
              <LogoHeader handleItemClick={this.handleItemClick} />

              <Menu.Menu position="right">
                <Menu.Item>
                  <Icon
                    size="large"
                    name="sidebar"
                    onClick={this.toggleSidebar}
                  />
                </Menu.Item>
              </Menu.Menu>
            </Menu>

            <Divider />
          </Grid.Column>

          <Grid.Column stretched width={16} only="tablet computer">
            <Menu secondary>
              <LogoHeader handleItemClick={this.handleItemClick} />

              <Menu.Menu position="right">
                {isAuthenticated
                  ? <MyPollsMenuItem
                      handleItemClick={this.handleItemClick}
                      active={activeMenuItem === 'my_polls'}
                    />
                  : ''}
                {isAuthenticated
                  ? <NewPollMenuItem
                      handleItemClick={this.handleItemClick}
                      active={activeMenuItem === 'new_poll'}
                    />
                  : ''}
                {isAuthenticated
                  ? <LogoutMenuItem logout={this.handleLogout} user={profile} />
                  : ''}
                {!isAuthenticated
                  ? <LoginMenuItem login={this.props.login} />
                  : ''}
              </Menu.Menu>
            </Menu>

            <Divider />
          </Grid.Column>
        </Grid>

        {/* Mobile sidebar */}
        <Sidebar
          as={Menu}
          animation="overlay"
          width="thin"
          visible={sidebarVisible}
          icon="labeled"
          vertical
        >
          <Menu.Item
            active={activeMenuItem === 'home'}
            as={Link}
            to="/"
            onClick={e => this.handleItemClick(e, { name: 'home' })}
          >
            <Icon name="home" />
            Home
          </Menu.Item>

          {isAuthenticated
            ? <Menu.Item
                active={activeMenuItem === 'bar chart'}
                as={Link}
                to="/my_polls"
                onClick={e => this.handleItemClick(e, { name: 'bar chart' })}
              >
                <Icon name="bar chart" />
                My Polls
              </Menu.Item>
            : ''}

          {isAuthenticated
            ? <Menu.Item
                active={activeMenuItem === 'checkmark box'}
                as={Link}
                to="/new_poll"
                onClick={e =>
                  this.handleItemClick(e, { name: 'checkmark box' })}
              >
                <Icon name="checkmark box" />
                New Poll
              </Menu.Item>
            : ''}

          {isAuthenticated
            ? <Menu.Item onClick={this.handleLogout}>
                <Icon name="sign out" />
                Sign out
              </Menu.Item>
            : <Menu.Item onClick={e => this.props.login(e)}>
                <Icon name="sign in" />
                Sign in
              </Menu.Item>}
        </Sidebar>
      </div>
    );
  }
}

export default Navigation;
