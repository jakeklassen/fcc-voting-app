import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Card,
  Container,
  Divider,
  Header,
  Icon,
  Label,
  Dimmer,
  Loader,
} from 'semantic-ui-react';

const renderPollPreview = poll =>
  <Card
    as={Link}
    style={{ margin: '1em auto', width: '100%' }}
    key={poll._id}
    to={`/polls/${poll._id}`}
  >
    <Card.Content>
      <Card.Header>{poll.title}</Card.Header>
      <Card.Description>
        <Label>
          <Icon name="line chart" /> {poll.votes.length}
        </Label>
      </Card.Description>
    </Card.Content>
  </Card>;

export default class Polls extends Component {
  state = {
    polls: [],
    fetching: true,
  };

  static propTypes = {
    polls: PropTypes.arrayOf(PropTypes.object),
    user: PropTypes.object.isRequired,
    fetchPolls: PropTypes.func.isRequired,
    mine: PropTypes.bool,
  };

  async componentDidMount() {
    const polls = await this.props.fetchPolls(this.props.mine);

    this.setState({
      ...this.state,
      polls,
      fetching: false,
    });
  }

  render() {
    const { polls, fetching } = this.state;

    if (fetching) {
      return (
        <Dimmer inverted active>
          <Loader inverted />
        </Dimmer>
      );
    }

    return (
      <div style={{ marginTop: '1em' }}>
        <Header as="h1" icon textAlign="center">
          <Icon name="bar chart" circular />
          <Header.Content>
            <Divider hidden />
            {this.props.mine ? 'Your polls' : 'Select a poll and vote'}
            <Divider hidden />
          </Header.Content>
        </Header>

        <Container textAlign="center" style={{ padding: '1em' }}>
          {polls.length === 0
            ? this.props.mine
              ? 'You have no polls'
              : 'There are currently no polls'
            : polls.map(renderPollPreview)}
        </Container>
      </div>
    );
  }
}
