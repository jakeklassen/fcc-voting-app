import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Icon,
  Input,
} from 'semantic-ui-react';
import { uniqBy } from 'lodash';
import 'whatwg-fetch';

function* idGenerator() {
  let count = 0;

  while (true) {
    count += 1;
    yield count;
  }
}

const ids = idGenerator();

const OPTION_PLACEHOLDER = 'Enter option text';

const optionFactory = () => ({
  id: ids.next().value,
  content: '',
});

const isOptionsValid = (options = []) => {
  // Require at least two options
  if (options.length < 2) return false;

  // Check for duplicate values.
  // Could be more strict, case checking etc.
  if (options.length !== uniqBy(options, 'content').length) {
    return false;
  }

  // Check for any empty values
  return options.filter(({ content }) => content.trim() === '').length === 0;
};

class NewPoll extends Component {
  state = {
    title: {
      isValid: false,
      value: '',
    },
    options: {
      isValid: false,
      // Require two options minimum
      value: [optionFactory(), optionFactory()],
    },
  };

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  renderOptions() {
    return this.state.options.value.map(({ content }, idx) =>
      <Form.Input
        key={idx}
        placeholder={OPTION_PLACEHOLDER}
        required
        value={content}
        onChange={(e, data) => this.updateOption(idx, data)}
        icon={
          <Icon
            name="close"
            link
            size="large"
            onClick={e => this.removeOption(e, idx)}
          />
        }
      />,
    );
  }

  addOption = () => {
    const options = this.state.options.value.concat(optionFactory());

    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        isValid: isOptionsValid(options),
        value: options,
      },
    });
  };

  removeOption(e, key) {
    const options = this.state.options.value.filter(
      (_option, idx) => idx !== key,
    );

    this.setState({
      ...this.state,
      options: {
        ...this.state.options,
        isValid: isOptionsValid(options),
        value: options,
      },
    });
  }

  updateOption(key, data) {
    const idx = this.state.options.value.findIndex(
      (_option, _idx) => key === _idx,
    );
    const option = this.state.options.value.find(
      (_option, _idx) => key === _idx,
    );

    if (option) {
      const options = this.state.options.value.map((_option, _idx) => {
        if (_idx !== idx) return _option;

        return {
          ...option,
          content: data.value,
        };
      });

      this.setState({
        ...this.state,
        options: {
          ...this.state.options,
          isValid: isOptionsValid(options),
          value: options,
        },
      });
    }
  }

  updateTitle = (e, { value }) =>
    this.setState({
      ...this.state,
      title: {
        ...this.state.title,
        isValid: value.trim().length > 3,
        value,
      },
    });

  handleSubmit = () => {
    fetch('/api/polls', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify({
        title: this.state.title.value,
        options: this.state.options.value.map(({ content }) => content),
      }),
    })
      .then(res => res.json())
      .then(({ poll }) => {
        this.props.history.push(`/polls/${poll._id}`);
      })
      .catch(error => console.error(error));
  };

  render() {
    return (
      <Container>
        <Header as="h2" icon textAlign="center">
          <Header.Content>
            <Divider hidden />
            Create a New Poll
          </Header.Content>
          <Header.Subheader>Complete the form below</Header.Subheader>
        </Header>

        <Container as="h4">
          A title and at least 2 unique options are required.
        </Container>

        <Divider hidden />

        <Form as={Container} textAlign="left">
          <Divider section horizontal>Title</Divider>
          <Form.Field>
            <Input placeholder="Title" onChange={this.updateTitle} />
          </Form.Field>
          <Divider section horizontal>Options</Divider>

          {this.renderOptions()}
        </Form>

        <Divider section horizontal hidden />

        <Button color="blue" size="large" onClick={this.addOption}>
          Add Option
        </Button>
        <Button
          positive
          size="large"
          onClick={this.handleSubmit}
          disabled={!this.state.options.isValid || !this.state.title.isValid}
        >
          Submit
        </Button>
      </Container>
    );
  }
}

export default withRouter(NewPoll);
