import express from 'express';
import passport from 'passport';
import compress from 'compression';
import helmet from 'helmet';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import session from 'express-session';
import morgan from 'morgan';
import createMongoStore from 'connect-mongo';
import boom from 'boom';

import initModels from './models';
import authentication from './authentication';
import routes from './routes';

export default ({ mongoose }) => {
  const models = initModels(mongoose);

  authentication(models.User);
  const MongoStore = createMongoStore(session);

  const app = express();

  app.disable('x-powered-by');
  if (process.env.NODE_ENV !== 'test') app.use(morgan('dev'));
  app.use(helmet());
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(compress());
  app.use(
    session({
      secret: process.env.SECRET,
      resave: false,
      saveUninitialized: false,
      unset: 'destroy',
      store: new MongoStore({
        mongooseConnection: mongoose.connection,
      }),
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());

  routes(app, models);

  // eslint-disable-next-line no-unused-vars
  app.use((err, req, res, next) => {
    let responseError;

    // Handle JWT auth error
    if (err.name === 'UnauthorizedError') {
      responseError = boom.unauthorized().output;
    }

    if (err.isBoom) {
      responseError = err.output;
    }

    if (!responseError) {
      responseError = boom.badImplementation().output;
    }

    res.status(responseError.statusCode).json(responseError);
  });

  return app;
};
