import debug from 'debug';
import mongoose from 'mongoose';
import initDatabase from './systems/database';
import app from './app';

mongoose.Promise = Promise;

const PORT = process.env.PORT;
const error = debug('app:error');
const log = debug('app:info');

initDatabase();

const server = app({ mongoose }).listen(PORT, err => {
  if (err) {
    error(err);
    server.close();
    process.exit();
  }

  log(`Listening @ http://localhost:${PORT}`);
});
