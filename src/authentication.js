import passport from 'passport';
import { Strategy as TwitterStrategy } from 'passport-twitter';

export default User => {
  passport.use(
    new TwitterStrategy(
      {
        consumerKey: process.env.TWITTER_CONSUMER_KEY,
        consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
        callbackURL: `/auth/twitter/callback`,
        // passReqToCallback: true,
      },
      async (token, tokenSecret, profile, done) => {
        let user = await User.findOne({ 'auth.twitter.id': profile.id });

        if (!user) {
          const newUser = new User({
            username: profile.username,
            auth: {
              twitter: {
                id: profile.id,
                username: profile.username,
              },
            },
          });

          user = await newUser.save();
        }

        done(null, user);
      },
    ),
  );

  // Used to serialize user into session
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  // Not required, left for reference
  // passport.deserializeUser((id, done) => {
  //   User.findById(id, (err, user) => {
  //     done(err, user);
  //   });
  // });
};
