export default mongoose => {
  const { Schema } = mongoose;

  const userSchema = new Schema(
    {
      username: {
        type: String,
        required: true,
        unique: true,
      },
      auth: {
        twitter: {
          id: Number,
          username: {
            type: String,
            index: true,
            displayName: String,
            photos: [Object],
          },
        },
      },
    },
    { timestamps: true },
  );

  return mongoose.model('User', userSchema);
};
