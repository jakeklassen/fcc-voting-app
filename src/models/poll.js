import { uniq } from 'lodash';

export default mongoose => {
  const { Schema } = mongoose;

  const voteSchema = new Schema(
    {
      createdAt: {
        type: Date,
        default: Date.now,
      },
      ip: {
        type: String,
        required: true,
      },
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        sparse: true,
      },
      value: {
        type: String,
        required: true,
      },
    },
    { _id: false },
  );

  const pollSchema = new Schema(
    {
      creator: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        index: true,
        required: true,
      },
      title: {
        type: String,
        required: true,
        minlength: 3,
        trim: true,
      },
      options: {
        type: [String],
        required: true,
        validate: [
          {
            validator: value => value.length >= 2,
            msg: 'Polls require a minmum of 2 options',
          },
          {
            validator: value => uniq(value).length === value.length,
            msg: 'Polls cannot contain duplicate options',
          },
        ],
      },
      // Validation of this is handle as route business logic
      votes: {
        type: [voteSchema],
        default: [],
      },
    },
    { timestamps: true },
  );

  return mongoose.model('Poll', pollSchema);
};
