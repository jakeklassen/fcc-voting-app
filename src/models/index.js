import createUserModel from './user';
import createPollModel from './poll';

export default mongoose => ({
  User: createUserModel(mongoose),
  Poll: createPollModel(mongoose),
});
