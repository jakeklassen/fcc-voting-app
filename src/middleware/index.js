import expressJwt from 'express-jwt';
import boom from 'boom';

export const isAuthenticated = expressJwt({
  secret: process.env.JWT_SECRET,
  getToken(req) {
    return req.cookies.token;
  },
});

export const getUser = expressJwt({
  secret: process.env.JWT_SECRET,
  credentialsRequired: false,
  getToken(req) {
    return req.cookies.token;
  },
});

export const isPollCreator = (req, res, next) => {
  if (req.poll.creator.equals(req.user._id)) {
    next();
  } else {
    const error = boom.unauthorized();
    next(error);
  }
};
