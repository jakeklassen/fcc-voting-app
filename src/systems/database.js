/**
 * Database service for dependency injection.
 */

import mongoose from 'mongoose';
import debug from 'debug';

mongoose.Promise = Promise;

const error = debug('app:error');
const log = debug('app:info');

export default (host, config = {}) => {
  const opts = {
    config: {
      autoIndex: true,
    },
    server: {
      auto_reconnect: true, // eslint-disable-line camelcase
      reconnectTries: Number.MAX_SAFE_INTEGER,
      reconnectInterval: 2500,
    },
    ...config,
  };

  mongoose.connect(host || process.env.MONGO_URL, opts);

  mongoose.connection.on('error', err => {
    error('Database error', err);
  });

  mongoose.connection.on('connected', () => {
    log('Database connected');
  });

  mongoose.connection.on('disconnected', () => {
    log('Database connection closed');
  });

  mongoose.connection.on('reconnected', () => {
    log('Database reconnected');
  });

  mongoose.connection.once('open', () => {
    log('Database connection open');
  });
};
