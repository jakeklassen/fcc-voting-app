import passport from 'passport';
import jsonwebtoken from 'jsonwebtoken';
import { omit } from 'lodash';
import { isAuthenticated } from '../../middleware';

export default router => {
  router.get('/auth/logout', isAuthenticated, (req, res) => {
    res.clearCookie('token');

    res.end();
  });

  router.get('/auth/twitter', passport.authenticate('twitter'));

  router.get(
    '/auth/twitter/callback',
    passport.authenticate('twitter'),
    (req, res) => {
      // Remove the session from the database
      req.session = null;

      // Create and set JWT cookie - strip the auth field
      const JWT_LIFE = process.env.JWT_LIFE || '24h';
      const token = jsonwebtoken.sign(
        omit(req.user.toObject(), ['auth']),
        process.env.JWT_SECRET,
        { expiresIn: JWT_LIFE },
      );

      res.cookie('token', token, { secure: false, httpOnly: true });
      res.redirect('/');
    },
  );
};
