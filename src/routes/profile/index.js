import { isAuthenticated } from '../../middleware';

export default router => {
  router.get('/api/profile', isAuthenticated, (req, res) => {
    res.json({
      ...req.user,
    });
  });
};
