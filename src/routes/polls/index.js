import boom from 'boom';
import mongoose from 'mongoose';
import { isAuthenticated, isPollCreator, getUser } from '../../middleware';

export default (router, models) => {
  const { Poll, User } = models;

  router.param('pollId', async (req, res, next, pollId) => {
    try {
      const poll = await Poll.findById(pollId);

      if (!poll) {
        const notFound = boom.notFound();
        next(notFound);
      } else {
        req.poll = poll;
        next();
      }
    } catch (error) {
      // Catch malformed ObjectId errors
      if (error instanceof mongoose.CastError) {
        const notFound = boom.notFound();
        next(notFound);
      } else {
        next(error);
      }
    }
  });

  router.post('/api/polls/:pollId/vote', getUser, async (req, res, next) => {
    const { option, custom = false } = req.body;

    // Guests cannot vote custom
    if (!req.user && custom) {
      const error = boom.forbidden('Guests cannot vote custom');
      return next(error);
    }

    const alreadyVoted = req.poll.votes.find(
      ({ ip, user }) => ip === req.ip || user.equals(req.user._id),
    );

    if (alreadyVoted) {
      const error = boom.forbidden("You've already voted");
      return next(error);
    }

    const optionExists = req.poll.options.find(_option => _option === option);
    if (!optionExists && !custom) {
      const error = boom.forbidden(`${option} is not a valid option`);
      return next(error);
    } else if (!optionExists && custom) {
      req.poll.options.push(option);
    }

    req.poll.votes.push({
      value: option,
      ip: req.ip,
      user: req.user ? req.user._id : undefined,
    });

    await req.poll.save();

    return res.json({
      poll: {
        ...req.poll.toObject(),
        canVote: false,
        canDelete: req.user ? req.poll.creator.equals(req.user._id) : false,
      },
    });
  });

  router
    .route('/api/polls/:pollId')
    .get(getUser, (req, res) => {
      const canVote = req.poll.votes.length > 0
        ? req.poll.votes.some(
            ({ ip, user }) =>
              (req.user && user === req.user.id) || ip === req.ip,
          ).length === 0
        : true;

      res.json({
        poll: {
          ...req.poll.toObject(),
          canVote,
          canDelete: req.user ? req.poll.creator.equals(req.user._id) : false,
        },
      });
    })
    .delete(isAuthenticated, isPollCreator, async (req, res) => {
      await req.poll.remove();

      res.end();
    });

  router
    .route('/api/polls')
    .get(getUser, async (req, res) => {
      const query = {};

      if (req.query.mine === 'true' && req.user) {
        query.creator = req.user._id;
      }

      const polls = await Poll.find(query).sort({ createdAt: -1 }).exec();
      res.json({ polls });
    })
    .post(isAuthenticated, async (req, res, next) => {
      const poll = new Poll({ ...req.body, creator: req.user._id });
      const validationError = poll.validateSync();

      if (validationError) {
        const error = boom.badRequest(validationError.message);
        return next(error);
      }

      const user = await User.findById(poll.creator);
      if (!user) {
        const error = boom.badRequest('Poll creator does not exist');
        return next(error);
      }

      return res.status(201).json({ poll: await poll.save() });
    });
};
