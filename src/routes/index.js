// Route composition file
import express from 'express';
import path from 'path';
import auth from './auth';
import profile from './profile';
import polls from './polls';

const staticPath = path.join(__dirname, '..', '..', 'public');

export default (router, models) => {
  auth(router);
  profile(router);
  polls(router, models);

  router.use(express.static(staticPath));

  router.get(/^\/(?!api).*/i, (req, res) => {
    // send all requests to index.html so browserHistory in React Router works
    res.sendFile(path.join(staticPath, 'index.html'));
  });
};
